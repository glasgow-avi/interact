#-------------------------------------------------
#
# Project created by QtCreator 2016-09-10T15:47:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Interact
TEMPLATE = app


SOURCES += main.cpp\
        chatwindow.cpp

HEADERS  += chatwindow.h

FORMS    += chatwindow.ui
