#include "chatwindow.h"
#include "ui_chatwindow.h"
#include <iostream>
#include <QMessageBox>
#include <unistd.h>

using namespace std;

ChatWindow::ChatWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChatWindow)
{
    ui->setupUi(this);
}

ChatWindow::~ChatWindow()
{
    delete ui;
}

void ChatWindow::on_sendButton_clicked()
{
    QString messageString = ui->messageEdit->toPlainText();
    const char* message = messageString.toStdString().c_str();
    ui->chatWidget->addItem(messageString);
    if(!fork())
    {
        //send Message to write to server
        exit(0);
    }
}
